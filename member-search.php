<?php get_header();?>

        <h2>Search Restult for Member <?php  echo $search=$_GET['s']; ?> </h2>

<?php 
    
    $post_type=$_GET['type']; 

    $member_args = array(
        'post_type' =>  'member',
        'posts_per_page'=>-1,        
        'meta_query'    =>  array(
                array(
                    'key'=>'member_name_english',
                    'value'=>$search,
                    'compare' =>'LIKE'
                ),
        ),
    );


    $memberSearchQuery = new WP_Query( $member_args );


     if($memberSearchQuery->have_posts() ) :
        
        while( $memberSearchQuery->have_posts() ) : $memberSearchQuery->the_post();   
            
            global $post;

            get_template_part( 'template-parts/content', 'member-search');

        endwhile;
        
        else :

    endif;

?>



<?php get_footer();?>