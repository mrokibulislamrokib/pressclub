<?php get_header();?>


        <h2> Search Restult For Committee <?php  echo $search=$_GET['s']; ?></h2>
<?php 
    
   

    
    $post_type=$_GET['type']; 

    $committee_args = array(
        'post_type' =>  'committee',
        'posts_per_page'=>-1,        
        'meta_query'    =>  array(
                array(
                    'key'=>'committee_name_english',
                    'value'=>$search,
                    'compare' =>'LIKE'
                ),
        ),
    );


    $committeeSearchQuery = new WP_Query( $committee_args );

    /*echo '<pre>';

    print_r($committeeSearchQuery);

    exit;*/


     if($committeeSearchQuery->have_posts() ) :
        
        while( $committeeSearchQuery->have_posts() ) : $committeeSearchQuery->the_post();   
            
            global $post;

            get_template_part( 'template-parts/content', 'committee-search');

        endwhile;
        
        else :

    endif;

?>


<?php get_footer();?>