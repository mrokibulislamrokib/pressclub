<!DOCTYPE html>
<html lang="en">
<head>
	<!--=== meta ===-->
	<title>Sylhet Zilla Press Club _ Home Page</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Document</title>
	  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
           <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head();?>
</head>
<body>


    <div class="page-wrapper">
        <div class="container page-box">
            <!-- **************************************-->
            <!-- **************Header**************-->
            <!-- **************************************-->
            <header>
                <nav>
                    <div class="nav-top">
                        <div class="row fix">
                            <div class="col-md-6 col-sm-6">
                                <div class="contact-member-login">
                                    <a href="<?php echo get_permalink( get_page_by_title( 'যোগাযোগ' ) )?>" class="contact-link">যোগাযোগ</a>
                                    <a href="<?php echo home_url(); ?>/wp-admin/" class="memcontact-member-loginbership-login">মেম্বারশীপ লগইন</a>
                                </div> 
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <?php get_search_form(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="nav-logo">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"> <img src="<?php echo get_theme_mod('logo_upload'); ?>" alt="" ></a>
                        <!-- ************** <h2 class="logo-text">SYLHET ZILLA PRESS CLUB</h2> **************-->
                    </div>

                    <div class="nav-menu navbar navbar-default">
                        <div class="navbar-header">
                            <!-- <span class="heading-2">Menu</span> -->
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav-menu" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <!-- <div class="collapse navbar-collapse" id="top-nav-menu"> -->
                            <?php 
                                $defaults = array(
                                    'theme_location'  => 'main-menu',
									'container_id'=>'top-nav-menu',
                                    'container' => 'div',
                                    'container_class' =>'navbar-collapse collapse',
                                    'items_wrap'      => '<ul id="menu" class="nav navbar-nav %2$s">%3$s</ul>',
                                );
                                wp_nav_menu( $defaults );
                            ?>
                        <!-- </div> -->
                    </div>
                </nav>
