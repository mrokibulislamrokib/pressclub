<?php get_header();?>
</header>

	
	<ul class="notice-list">
	<?php
		if(have_posts()) :
			while ( have_posts() ) : the_post();	
	?>
		<?php
					get_template_part( 'template-parts/content', 'search' );
				endwhile; // End of the loop.
			else : ?>
				<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', '' ); ?></p>
				<?php
					get_search_form();
			endif;
		?>
	</ul>

<?php get_footer();?>