<?php
	global $post;
	$notice_time=get_post_meta($post->ID, 'notice_time',true);
	$notice_location=get_post_meta($post->ID, 'notice_location',true);
	$notice_subject=get_post_meta($post->ID, 'notice_subject',true);
?>

<li class="single-notice">
    <div class="content-slider">
        <h3 class="noticeboard-heading"><?php the_title();?></h3>
        <p class="time"><?php echo $notice_time;?></p>
        <p class="place">স্থান:<?php echo $notice_location;?></p>
        <button data-noticeid="<?php echo $post->ID;?>"  class="subject quicknoticeview">সংবাদ সম্মেলনের বিষয়: <?php echo $notice_subject;?></button>
    </div>
</li>