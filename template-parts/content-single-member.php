<?php
    global $post;

    $member_name_english=get_post_meta($post->ID, 'member_name_english',true);
    $member_press_designation_english=get_post_meta($post->ID, 'member_press_designation_english',true);
    $member_fb=get_post_meta($post->ID, 'member_fb',true);


    $member_name=get_post_meta($post->ID, 'member_name',true);
    $member_fname=get_post_meta($post->ID, 'member_fname',true);
    $member_rmedia=get_post_meta($post->ID, 'member_rmedia',true);
    $member_status=get_post_meta($post->ID, 'member_status',true);
    $member_birth=get_post_meta($post->ID, 'member_birth',true);
    $member_present_address=get_post_meta($post->ID, 'member_present_address',true);
    $member_pemanent_address=get_post_meta($post->ID, 'member_pemanent_address',true);
    $member_email=get_post_meta($post->ID, 'member_email',true);
    $member_personal_cell=get_post_meta($post->ID, 'member_personal_cell',true);
    $member_personal_office=get_post_meta($post->ID, 'member_personal_office',true);
    $member_job=get_post_meta($post->ID, 'member_job',true);
    $member_education=get_post_meta($post->ID, 'member_education',true);
    $member_blood=get_post_meta($post->ID, 'member_blood',true);
    $member_other_organization=get_post_meta($post->ID, 'member_other_organization',true); 	
?>



 <div class="row bio">
    <div class="col-md-3 bio-left">
        <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail('pressclub_enlistedmedia_image');
            } 
        ?>
        <h3 class="h3-style-bio"><?php echo $member_name_english;?></h3>
        <p class="bio"> <?php echo $member_press_designation_english;?></p>
        <a href="#" class="link-text"><i class="fa fa-facebook-square" aria-hidden="true"></i>/<?php echo $member_fb;?></a>
    </div>
    <div class="col-md-9">
        <table class="table">
            <tbody>
                <tr>
                    <td class="right">Full Name (Bangla)</td>
                    <td><?php echo $member_name;?></td>
                </tr>
                <tr>
                    <td class="right-2">Father's Name / Spouse Name </td>
                    <td><?php echo $member_fname;?></td>
                </tr>
                <tr>
                    <td class="right">Represented Media</td>
                    <td><?php echo $member_rmedia;?></td>
                </tr>
                <tr>
                    <td class="right-2">Membership Status</td>
                    <td><?php echo $member_status;?></td>
                </tr>
                <tr>
                    <td class="right">Date of Birth </td>
                    <td> <?php echo $member_birth;?> </td>
                </tr>
                <tr>
                    <td class="right-2">Present Address</td>
                    <td><?php echo $member_present_address;?></td>
                </tr>
                <tr>
                    <td class="right">Permanent Address</td>
                    <td><?php echo $member_pemanent_address;?></td>
                </tr>
                <tr>
                    <td class="right-2">Email </td>
                    <td><?php echo $member_email;?></td>
                </tr>
                <tr>
                    <td class="right">Cell (Personal)</td>
                    <td><?php echo $member_personal_cell;?></td>
                </tr>
                <tr>
                    <td class="right-2"> Cell (Office)</td>
                    <td> <?php echo $member_personal_office;?></td>
                </tr>
                <tr>
                    <td class="right">Job History</td>
                    <td><?php echo $member_job;?></td>
                </tr>
                <tr>
                    <td class="right-2"> Education</td>
                    <td><?php echo $member_education;?></td>
                </tr>
                <tr>
                    <td class="right">Blood Group</td>
                    <td><?php echo $member_blood;?></td>
                </tr>
                <tr>
                    <td class="right-2">Other Organizations (if any)</td>
                    <td> <?php echo $member_other_organization;?> </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>