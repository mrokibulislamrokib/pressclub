<?php
 	global $post;
    $thumb_id = get_post_thumbnail_id();
    $thumb_url = wp_get_attachment_image_src($thumb_id,'pressclub_slider_image', true);
 ?>

<div class="item" style="background-image:url(<?php  echo $thumb_url[0];?>)">
    <div class="info-box">
        <div class="item-dtls">
            <a href="<?php the_permalink();?>" class="heading-4 item-title"><?php the_title(); ?></a>
        </div>
    </div>
</div>