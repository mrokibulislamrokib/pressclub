<?php
    global $post;
    $member_name=get_post_meta($post->ID, 'member_name',true);
    $member_press_designation_english=get_post_meta($post->ID, 'member_press_designation_english',true);
?>



                    <div class="item">
                        <div class="item-img">
                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('pressclub_member_image');
                                } 
                            ?>
                        </div>
                        <div class="item-dtls">
                            <div class="name-date">
                             <a href="<?php the_permalink();?>" class="link-text">    <p class="p-text name"><?php echo $member_name;?></p> </a>
                                <p class="p-text date"><?php echo $member_press_designation_english;?></p>
                            </div>
                            <div class="dtls">
                                <p class="p-text">
                                    <a href="<?php the_permalink();?>" class="link-text">জীবনবৃত্তান্ত</a>
                                </p>
                            </div>
                        </div>
                    </div>
  