<?php

 	global $post;              
	$thumb_id = get_post_thumbnail_id();
	$thumb_url = wp_get_attachment_image_src($thumb_id,'pressclub_slider_image', true);
	$slider_link=get_post_meta($post->ID, 'slider_link',true);
                    
?>            
<div class="item" style="background-image:url(<?php  echo $thumb_url[0];?>)">
    <div class="item-dtls">
        <div class="row fix">
            <div class="col-md-12 p-none">
                <a href="<?php echo $slider_link;?>" target="_blank" class="heading-1 item-title"><?php the_title();?></a>
            </div>
        </div>
    </div>
</div>