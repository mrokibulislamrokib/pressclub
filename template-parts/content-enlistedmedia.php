<div class="item">
    <button class="img-box">
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('pressclub_enlistedmedia_image');
        } ?>
    </button>
</div>
