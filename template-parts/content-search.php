<?php 
    
    $type=get_post_type( get_the_ID() );

	if($type=='activity'){
		$activity_link=get_post_meta($post->ID, 'activity_link',true);
	} 
?>
<li class="single-notice">
	<div class="content-slider">
    	<h3 class="noticeboard-heading"><?php the_title();?></h3>
    	<?php 
    		if($type=='notice'){ ?>
    		<a data-noticeid="<?php echo $post->ID;?>"  class="subject quicknoticeview">Read More</a>
    	<?php	} else if($type=='activity'){?>

    		<a class="news-text-2" href="<?php echo $activity_link; ?>">Read More</a>

    	<?php } ?>
    	
    </div>
</li>
