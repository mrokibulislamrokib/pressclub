<?php

$activity_link=get_post_meta($post->ID, 'activity_link',true);?>

    <div class="col-md-4 col-sm-6">
        <div class="media news-box-2">
            <a href="<?php the_permalink();?>" target="_blank" class="pull-left">
                <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail('pressclub_enlistedmedia_image');
                    } 
                ?>
                <div class="media-body">
                    <h4 class="media-heading news-header"><?php the_title();?>
                     </h4>
                    <p class="news-text-2"><?php the_content();?></p>
                </div>
            </a>
        </div>
    </div>


