<?php
	if ( has_post_thumbnail() ) {
	    the_post_thumbnail('pressclub_enlistedmedia_image');
	} 
?>
<h3 class="news-heading-h3"><?php the_title();?></h3>
<p class="news-text"><?php the_content();?></p>