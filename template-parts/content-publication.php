<?php
    
    global $post;
    $publication_name=get_post_meta($post->ID, 'publication_name',true);
    $publication_writter=get_post_meta($post->ID, 'publication_writter',true);


    $dpwork_thumbinail_img_id = get_post_meta( $post->ID, '_dpwork_thumbinail_img_id', true );
    $pdf="";
    if($dpwork_thumbinail_img_id!==""){
        $image_link = wp_get_attachment_url( $dpwork_thumbinail_img_id,"full");
    //    $pdf='<a href="'.$image_link.'" alt="" style="max-width:100%;" download>Download pdf</a>';
    }
?>

<li class="single-publication">
    <div class="item-img">
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('pressclub_publication_image');
        } ?>
    </div>
    <div class="item-info">
        <h3 class="heading-3"><?php echo $publication_name;?></h3>
        <p class="p-text"><?php echo $publication_writter;?></p>
        <a href="<?php echo $image_link; ?>" alt="" class="link-text" download>Download PDF</a>

    </div>
</li>