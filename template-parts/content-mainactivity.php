            <div class="item">
                <div class="item-img">
        
                    <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail('pressclub_enlistedmedia_image');
                        } 
                    ?>
                </div>
                <div class="item-dtls">
                    <div class="name-date">
                        <p class="p-text name"><?php the_title();?></p>
                        <p class="p-text date">১০ই নভেম্বর ২০১৭</p>
                    </div>
                    <div class="dtls">
                        <p class="p-text">
                            <a href="<?php the_permalink(); ?>" class="link-text">বিস্তারিত</a>
                        </p>
                    </div>
                </div>
            </div>