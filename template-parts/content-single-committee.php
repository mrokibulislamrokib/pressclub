<?php
    global $post;

    $committee_name_english=get_post_meta($post->ID, 'committee_name_english',true);
    $committee_press_designation_english=get_post_meta($post->ID, 'committee_press_designation_english',true);
    $committee_fb=get_post_meta($post->ID, 'committee_fb',true);

    $committee_name=get_post_meta($post->ID, 'committee_name',true);
    $committee_fname=get_post_meta($post->ID, 'committee_fname',true);
    $committee_rmedia=get_post_meta($post->ID, 'committee_rmedia',true);
    $committee_status=get_post_meta($post->ID, 'committee_status',true);
    $committee_birth=get_post_meta($post->ID, 'committee_birth',true);
    $committee_present_address=get_post_meta($post->ID, 'committee_present_address',true);
    $committee_pemanent_address=get_post_meta($post->ID, 'committee_pemanent_address',true);
    $committee_email=get_post_meta($post->ID, 'committee_email',true);
    $committee_personal_cell=get_post_meta($post->ID, 'committee_personal_cell',true);
    $committee_personal_office=get_post_meta($post->ID, 'committee_personal_office',true);
    $committee_job=get_post_meta($post->ID, 'committee_job',true);
    $committee_education=get_post_meta($post->ID, 'committee_education',true);
    $committee_blood=get_post_meta($post->ID, 'committee_blood',true);
    $committee_other_organization=get_post_meta($post->ID, 'committee_other_organization',true);
?>



 <div class="row bio">
    <div class="col-md-3 bio-left">
        <?php
            if ( has_post_thumbnail() ) {
                the_post_thumbnail('pressclub_enlistedmedia_image');
            } 
        ?>
        <h3 class="h3-style-bio"><?php echo $committee_name_english;?></h3>
        <p class="bio"> <?php echo $committee_press_designation_english;?></p>
        <a href="#" class="link-text"><i class="fa fa-facebook-square" aria-hidden="true"></i>/<?php echo $committee_fb;?></a>
    </div>
    <div class="col-md-9">
        <table class="table">
            <tbody>
                <tr>
                    <td class="right">Full Name (Bangla)</td>
                    <td><?php echo $committee_name;?></td>
                </tr>
                <tr>
                    <td class="right-2">Father's Name / Spouse Name </td>
                    <td><?php echo $committee_fname;?></td>
                </tr>
                <tr>
                    <td class="right">Represented Media</td>
                    <td><?php echo $committee_rmedia;?></td>
                </tr>
                <tr>
                    <td class="right-2">committeeship Status</td>
                    <td><?php echo $committee_status;?></td>
                </tr>
                <tr>
                    <td class="right">Date of Birth </td>
                    <td> <?php echo $committee_birth;?> </td>
                </tr>
                <tr>
                    <td class="right-2">Present Address</td>
                    <td><?php echo $committee_present_address;?></td>
                </tr>
                <tr>
                    <td class="right">Permanent Address</td>
                    <td><?php echo $committee_pemanent_address;?></td>
                </tr>
                <tr>
                    <td class="right-2">Email </td>
                    <td><?php echo $committee_email;?></td>
                </tr>
                <tr>
                    <td class="right">Cell (Personal)</td>
                    <td><?php echo $committee_personal_cell;?></td>
                </tr>
                <tr>
                    <td class="right-2"> Cell (Office)</td>
                    <td> <?php echo $committee_personal_office;?></td>
                </tr>
                <tr>
                    <td class="right">Job History</td>
                    <td><?php echo $committee_job;?></td>
                </tr>
                <tr>
                    <td class="right-2"> Education</td>
                    <td><?php echo $committee_education;?></td>
                </tr>
                <tr>
                    <td class="right">Blood Group</td>
                    <td><?php echo $committee_blood;?></td>
                </tr>
                <tr>
                    <td class="right-2">Other Organizations (if any)</td>
                    <td> <?php echo $committee_other_organization;?> </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>