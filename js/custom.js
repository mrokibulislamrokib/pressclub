jQuery(document).ready(function($) {
    


/*(function($){*/

	"use strict";



	/**

    * ----------------------------------------------

    * owl-carousel custom-slider

    * ----------------------------------------------

    */


    $(".hero-slider").owlCarousel({

        items:1,

        dots:true,       

        //autoplay: true,        

        nav:true,

        navSpeed: 800,

        dotsSpeed: 800,

        autoplaySpeed: 800,

        navText:['<img src=' + previmage + '>', '<img src=' + nextimage + '>'],
        loop: true,

        responsive : {

            // breakpoint from 0 up

            0 : {

                dots:false,

                nav:false,

                autoplay: false

            },

            // breakpoint from 480 up

              480 : {

                dots:false,

                nav:false,

                autoplay: true

              },

            // breakpoint from 768 up

            768 : {

                dots:true,

                nav:true,

                autoplay: false

            }

        },

    });



    /**

    * ----------------------------------------------

    * owl-carousel custom-slider

    * ----------------------------------------------

    */

    $(".activities-slider").owlCarousel({

        items:2,

        // center: true,

        //autoplay: true, 
        margin : 30,       

        nav:true,

        navSpeed: 800,

        autoplaySpeed: 800,

        navText:['<img src=' + previmage + '>', '<img src=' + nextimage + '>'],
        loop: true,

        responsive : {

            // breakpoint from 0 up

            0 : {

                items:1,

                dots:false,

                nav:false,

                autoplay: false

            },

            // breakpoint from 480 up

              480 : {

                items:1,

                dots:false,

                nav:false,

                autoplay: true

              },

            // breakpoint from 768 up

            768 : {

                dots:false,

                nav:true,

                autoplay: false

            },

            // breakpoint from 768 up

            991 : {
                items:2,

                dots:false,

                nav:true,

                autoplay: false

            }

        },

    });	/**

    * ----------------------------------------------

    * owl-carousel custom-slider

    * ----------------------------------------------

    */

    $(".activities-slider2").owlCarousel({

        items:5,

        // center: true,

        //autoplay: true, 
        margin : 30,       

        nav:true,

        navSpeed: 800,

        autoplaySpeed: 800,


        navText:['<img src=' + previmage + '>', '<img src=' + nextimage + '>'],
        /*loop: true,*/

        responsive : {

            // breakpoint from 0 up

            0 : {

                items:1,

                dots:false,

                nav:false,

                autoplay: false

            },

            // breakpoint from 480 up

              480 : {

                items:2,

                dots:false,

                nav:false,

                autoplay: true

              },

            // breakpoint from 768 up

            768 : {
                items:3,

                dots:false,

                nav:true,

                autoplay: false

            },

            // breakpoint from 768 up

            991 : {
                items:5,

                dots:false,

                nav:true,

                autoplay: false

            }

        },

    });

    /**
    * ----------------------------------------------
    * owl-carousel sponsor-carousel
    * ----------------------------------------------
    */
    $(".partners-carousel").owlCarousel({
        items:5,
        dots:false,       
        // autoplay: true,        
        nav:false,
        navSpeed: 800,
        dotsSpeed: 800,
        autoplaySpeed: 800,
        margin: 20,
        loop: true,
        responsive : {
            // breakpoint from 0 up
            0 : {
                items:2
            },
            // breakpoint from 480 up
              480 : {
                items:3
              },
            // breakpoint from 768 up
            768 : {
                items:5
            }
        },
    });

    /**

    * ----------------------------------------------

    * owl-carousel notice-board

    * ----------------------------------------------

    */

    $(".notice-board-carousel").owlCarousel({

        items:5,

        dots:false,       

        autoplay: true,        

        nav:false,

        navSpeed: 800,

        dotsSpeed: 800,

        autoplaySpeed: 800,

        margin: 20,

        loop: true,

        responsive : {

            // breakpoint from 0 up

            0 : {

                items:1

            },

            // breakpoint from 480 up

              480 : {

                items:3

              },

            // breakpoint from 768 up

            991 : {

                items:5

            }

        },

    });



	/**

    * ----------------------------------------------

    * owl-carousel custom-slider

    * ----------------------------------------------

    */

    $(".gallery-slider").owlCarousel({

        items:1,

        dots:true,       

        //autoplay: true,        

        nav:true,

        navSpeed: 800,

        dotsSpeed: 800,

        autoplaySpeed: 800,

        navText:['<img src=' + previmage + '>', '<img src=' + nextimage + '>'],
        loop: true,

        responsive : {

            // breakpoint from 0 up

            0 : {

                dots:false,

                nav:false,

                autoplay: false

            },

            // breakpoint from 480 up

              480 : {

                dots:false,

                nav:false,

                autoplay: true

              },

            // breakpoint from 768 up

            768 : {

                dots:true,

                nav:true,

                autoplay: false

            }

        },

    });



	/**

    * ----------------------------------------------

    * owl-carousel custom-slider

    * ----------------------------------------------

    */


    $(".website-link-slider").owlCarousel({

        items:6,

        //autoplay: true,        

        nav:true,

        navSpeed: 800,

        autoplaySpeed: 800,

        navText:['<img src=' + previmage + '>', '<img src=' + nextimage + '>'],

        loop: true,

        margin: 0,

        responsive : {

            // breakpoint from 0 up

            0 : {

                items:2,

                dots:false,

                nav:false,

                autoplay: false

            },

            // breakpoint from 480 up

              480 : {

                items:3,

                dots:false,

                nav:false,

                autoplay: true

              },

            // breakpoint from 768 up

            768 : {

                items:3,

                dots:false,

                nav:true,

                margin: 10,

                autoplay: false

            },

            // breakpoint from 768 up

            991 : {

                items:6,

                dots:false,

                nav:true,

                autoplay: false

            }

        },

    });



    /**

     * ----------------------------------------------

     * Scrool top js

     * ----------------------------------------------

     */

    var scrollTop = $(".scroll-top");

    $(window).scroll(function() {

        var topPos = $(this).scrollTop();

        if (topPos > 100) {

            $(scrollTop).css("opacity", "1");

        } else {

            $(scrollTop).css("opacity", "0");

        }

    });

    $(scrollTop).click(function() {

        $('html, body').animate({

            scrollTop: 0

        }, 800);

        return false;

    });


    $(document).on('click', '.quicknoticeview', function(event) {
        event.preventDefault();
        var noticeid = $(this).data('noticeid');
        console.log(noticeid);
        jQuery.ajax({
          url: ajaxurl,
          type: 'POST',
          dataType:'json',
          data: {'action': 'notice_quick_view','noticeid':noticeid},
          complete: function(xhr, textStatus) {
            //called when complete
          },
          success: function(data, textStatus, xhr) {
            $('#mymodal2').find('.noticeboard-heading').text(data.title);          
            $('#mymodal2').find('.time').text(data.notice_time);
            $('#mymodal2').find('.place').text(data.notice_location);
            $('#mymodal2').find('.subject').text(data.notice_subject);
            $('#mymodal2').modal('show');
          },
          error: function(xhr, textStatus, errorThrown) {
            //called when there is an error
          }
        });
        
    });

    $(document).on('click', '.quickbrandview', function(event) {
        event.preventDefault();
        var brandid = $(this).data('brandid');
        console.log(brandid);
        jQuery.ajax({
          url: ajaxurl,
          type: 'POST',
          dataType:'json',
          data: {'action': 'brand_quick_view','brandid':brandid},
          complete: function(xhr, textStatus) {
            //called when complete
          },
          success: function(data, textStatus, xhr) {
            $('.brand_title').text(data.title);
            $('.brand_content').text(data.content);
            $('.brand-logo').attr({
                src: data.image
            });
            $('#mymodal').modal('show');
          },
          error: function(xhr, textStatus, errorThrown) {
            //called when there is an error
          }
        });
        
    });




(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



/*})(jQuery); */

});