<?php /* Template Name: Publication Template */ get_header();?>
</header>

<div class="publications-body">
    <div class="section-title">
        <h4 class="header-style-h4 bottom-style">প্রেসক্লাব প্রকাশনাসমূহ</h4>
    </div>



 	<?php 
 			$Publication_page_section=get_theme_mod('Publication_page_section');
 			if(empty($Publication_page_section)){
 				$Publication_page_section=12;
 			}
 	?>

    <?php get_rok_post('publication',$Publication_page_section,'true');?>

</div>

<div class="white-space"></div>

<?php get_footer();?>