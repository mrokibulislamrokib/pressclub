<?php get_header();?>
</header>
  <div class="row fix">
            <div class="col-md-4 activity-left">
                <h4 class="news-heading">সাম্প্রতিক কার্যক্রম</h4>
                <ul class="list-unstyled">
                    <?php
                        $query_args = array('post_type' => 'activity','posts_per_page' => -1);
                        $the_query = new WP_Query( $query_args );
                        if($the_query->have_posts()): 
                            while($the_query->have_posts()) : $the_query->the_post(); 
                                global $post;
                    ?>
    
                    <li> <a href="<?php the_permalink();?>"><i class="fa fa-check-square spacer" aria-hidden="true"></i> <?php the_title();?> </a> </li>

                    <?php endwhile; endif; ?>

                </ul>
            </div>
            <div class="col-md-8 activity-right">
            	<?php 
					if(have_posts()):
						while ( have_posts() ) : the_post(); 	
							get_template_part( 'template-parts/content', 'single-activity');
						endwhile; 
					endif;
				?> 
            </div>
        </div>


<?php get_footer();?>