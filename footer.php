            <!-- **************************************-->
            <!-- **************Footer**************-->
            <!-- **************************************-->
            <footer>
                <div class="footer-top">
                    <nav> 
                            <?php  
                                $defaults = array(
                                    'theme_location'  => 'footer',
                                    'items_wrap'      => '<ul id="menu" class="list-unstyled f-menu %2$s">%3$s</ul>',
                                );
                                wp_nav_menu( $defaults );
                            ?> 

                    </nav>
                    <ul class="list-unstyled social-icons">
                        <li><a href="<?php echo get_theme_mod('fb_section');?>" class="single-icon"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('twitter_section');?>" class="single-icon"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('youtube_section');?>" class="single-icon"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('linkedin_section');?>" class="single-icon"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="<?php echo get_theme_mod('google_section');?>" class="single-icon"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
                <hr class="style-two">
                <div class="footer-bottom">
                    <p class="p-text">&copy;2017 Sylhet Zilla Press Club, All Rights Reserved</p>
                    <p class="p-text">
                        Designed and Devoloped by 
                        <a href="#" target="_blank">Team-AJaX</a>
                    </p>
                </div>
            </footer>

            <!-- **************************************-->
            <!-- **************All Modal**************-->
            <!-- **************************************-->

            <div class="modal  fade all-modal" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-close"></span></button>
                        </div>
                        <div class="clear"></div>
                        <div class="modal-body">
                            <div class="row fix">
                                <div class="col-md-5 col-sm-5 brand-name">
                                    <img class="brand-logo" src="" alt="brand name">
                                    <h2 class="heading-2 brand_title"></h2>
                                </div>
                                <!-- <hr class="style-two"> -->
                                <div class="col-md-7 col-sm-7 brand-info">
                                    <p class="p-text brand_content"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="modal  fade all-modal" id="mymodal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="fa fa-close"></span></button>
                        </div>
                        <div class="clear"></div>
                        <div class="modal-body">
                            <div class="row fix">
                                <div class="col-md-5 col-sm-5 brand-name">
                                    <h2 class="heading-2 brand_title noticeboard-heading"></h2>
                                </div>
                                <!-- <hr class="style-two"> -->
                                <div class="col-md-7 col-sm-7 brand-info">
                                    <p class="time"></p>
                                    <p class="place"></p>
                                    <a class="subject"></a>
                                </div>      
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- **************************************-->
            <!-- **************Footer*****************-->
            <!-- **************************************-->
            <div class="scroll-top" title="Scroll to top">
                <i class="fa fa-arrow-up"></i>
            </div>
        </div>
    </div>
<?php wp_footer();?>
</body>
</html>


