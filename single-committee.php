<?php get_header();?>
</header>


<?php 
	if(have_posts()):
		while ( have_posts() ) : the_post(); 	
			get_template_part( 'template-parts/content', 'single-committee');
		endwhile; 
	endif;
?>
                
<?php get_template_part( 'committee', 'searchform' ); ?>


<?php get_footer();?>