<?php /* Template Name: Partner Template */ get_header();?>
</header>

<div class="white-space"></div>

<div class="partners">
    <div class="section-title">
        <h4 class="header-style-h4 bottom-style">কর্পোরেট পার্টনারস</h4>
    </div>

    <?php  
      get_rok_post('partner',-1,'false',"<div class=\"owl-carousel partners-carousel\">","</div>");
    ?>
</div>
<?php get_footer();?>