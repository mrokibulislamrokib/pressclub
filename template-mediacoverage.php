<?php  /* Template Name: mediacoverage Template */ get_header();?>
</header>

<div class="row media-coverage">

 	<?php 
 			$mediacoverage_page_section=get_theme_mod('mediacoverage_page_section');
 			if(empty($mediacoverage_page_section)){
 				$mediacoverage_page_section=12;
 			}
 	?>

    <?php get_rok_post('mediacoverage',$mediacoverage_page_section,'true');?>

</div>

 <div class="white-space"></div>
<?php get_footer();?>