<?php /* Template Name: Notice Template */ get_header();?>
</header>

 <div class="white-space"></div>

 <div class="notice-list">

 	<?php 
 			$notice_page_section=get_theme_mod('notice_page_section');
 			if(empty($notice_page_section)){
 				$notice_page_section=12;
 			}
 	?>

    <?php get_rok_post('notice',$notice_page_section,'true');?>

</div>

<div class="white-space"></div>


<?php get_footer();?>