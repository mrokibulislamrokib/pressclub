<form method="get" id="advance-searchform" role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="row search-bar">
    <div class="col-md-12">
        <div class="input-group" id="adv-search">
            
                <input type="text" class="form-control" name="s" placeholder="Search for Committee" />
                <input type="hidden" name="type" value="committee">
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn btn-primary"><span class="fa fa-search" aria-hidden="true"></span></button>
                    </div>
                </div>
            
        </div>
    </div>
</div>

</form>