<?php get_header();?>
    <div class="header-slider">
       <?php get_rok_post('slider',5,'false',"<div class=\"owl-carousel hero-slider\">","</div>"); ?>
    </div>
</header>

            <!-- **************************************-->
            <!-- ************** president-spech **************-->
            <!-- **************************************-->
            <section class="spech-section">
                <div class="section-title">
                    <h4 class="heading-4 title-style">সভাপতি ও সাধারণ সম্পাদকের বক্তব্য</h4>
                </div>
                
                <div class="row fix spech-info">
                    <div class="col-md-4 col-sm-5 pull-right ">
                        <div class="spech-of">
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg" alt="">
                            </div>
                            <div class="info-box">
                                <p class="p-text user-name"> <?php echo get_theme_mod('name_president'); ?> </p>
                                <p class="p-text user-designation"><?php echo get_theme_mod('desgination_president'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-7 pull-left spech-text">
                        <p class="p-text">
                            <?php echo get_theme_mod('spech_president'); ?>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row fix spech-info">
                    <div class="col-md-4 col-sm-5">
                        <div class="spech-of">
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg" alt="">
                            </div>
                            <div class="info-box">
                                <p class="p-text user-name"><?php echo get_theme_mod('name_secratary'); ?></p>
                                <p class="p-text user-designation"><?php echo get_theme_mod('desgination_secratary'); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-7 spech-text">
                        <p class="p-text"><?php echo get_theme_mod('spech_secratary'); ?>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>
            </section>

            <!-- **************************************-->
            <!-- ************** Club Activities **************-->
            <!-- **************************************-->
            <section class="club-activities">
                <div class="section-title">
                    <h4 class="heading-4 title-style">ক্লাব কার্যক্রম</h4>
                </div>
        
        <?php 
        get_rok_post('activity',-1,'false',"<div class=\"owl-carousel activities-slider\">","</div>",$template_name='mainactivity');
        ?>

            </section>

            <!-- **************************************-->
            <!-- **************partners**************-->
            <!-- **************************************-->
            <div class="partners">
                <div class="section-title">
                    <h4 class="heading-4 title-style">কর্পোরেট পার্টনারস</h4>
                </div>

                    <?php  

                get_rok_post('partner',-1,'false',"<div class=\"owl-carousel partners-carousel\">","</div>",$template_name='mainpartner');  ?>

            </div>

            <!-- **************************************-->
            <!-- ************** gallery **************-->
            <!-- **************************************-->
            <section class="gallery">
                <div class="section-title">
                    <h4 class="heading-4 title-style">গ্যালারী</h4>
                </div>

                <div class="row fix gallery-info">
                    <?php  get_rok_post('activity',5,'false',"<div class=\"col-md-9 col-sm-7 pl-none\"> <div class=\"owl-carousel gallery-slider\">","</div></div>",$template_name='maingallery'); ?>
                    <div class="col-md-3 col-sm-5 pr-none">
                        <div class="fb-plgin">
                            
                            <div class="fb-page" data-href="<?php echo get_theme_mod( 'fb_page_section');?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/JoinHelloSylhet" class="fb-xfbml-parse-ignore"><a href="<?php echo get_theme_mod( 'fb_page_section');?>">Sylhet Press Club</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- **************************************-->
            <!-- **************notice-board**************-->
            <!-- **************************************-->
            <div class="notice-board">
                <div class="section-title">
                    <h4 class="heading-4 title-style">নোটিশ বোর্ড</h4>
                </div>
                 <?php  get_rok_post('notice',-1,'false'," <div class=\"owl-carousel notice-board-carousel\">","</div>",$template_name='mainnotice'); ?>
            </div>

            <!-- **************************************-->
            <!-- ************** Website link**************-->
            <!-- **************************************-->
            <section class="website-link">
                <div class="section-title">
                    <h4 class="heading-4 title-style">গুরুত্বপূর্ণ ওয়েবসাইট লিংক</h4>
                </div>
                <?php  get_rok_post('importantweb',-1,'false'," <div class=\"owl-carousel website-link-slider\">","</div>",$template_name='mainimportantweb'); ?>
            </section><!--  -->
<?php get_footer();?>