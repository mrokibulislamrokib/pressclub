<?php

function pressclub_setup(){
	load_theme_textdomain( 'pressclub' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'pressclub-featured-image', 2000, 1200, true );
	add_image_size( 'pressclub-thumbnail-avatar', 100, 100, true );
	add_image_size( 'pressclub_partner_image', 200, 100, true );
	add_image_size( 'pressclub_member_image', 200, 200, true );
	add_image_size( 'pressclub_enlistedmedia_image', 200, 100, true );
	add_image_size( 'pressclub_publication_image', 357, 476, true );
	add_image_size( 'pressclub_slider_image', 1200, 1200, true );
	register_nav_menus( array(
		'top'    => __( 'Top Menu', '' ),
		'main-menu' => __( 'Main Menu', '' ),
		'footer' => __( 'Footer Menu', '' ),
	) );

	add_theme_support( 'post-formats', array('aside','image','video','quote','link','gallery','audio',));
	add_theme_support( 'html5', array('comment-form','comment-list','gallery','caption',));

}

add_action( 'after_setup_theme', 'pressclub_setup');


function pressclub_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'pressclub' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'pressclub' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'pressclub' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'pressclub' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'pressclub' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'pressclub' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pressclub_widgets_init' );

function pressclub_scripts(){
    wp_enqueue_style( 'pressclub_css',get_template_directory_uri() .'/css/bootstrap.min.css' );
    wp_enqueue_style( 'pressclub_awesome_css',get_template_directory_uri() .'/css/font-awesome.min.css' );
    wp_enqueue_style( 'pressclub_carousel_css',get_template_directory_uri() .'/css/owl.carousel.css' );
    wp_enqueue_style( 'pressclub_main_css',get_template_directory_uri() .'/css/main.css' );
	wp_enqueue_style( 'pressclub_css_style',get_template_directory_uri() .'/style.css' );
	wp_enqueue_script("jquery");
	wp_enqueue_script( 'pressclub_bootstrap_js',get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_script( 'pressclub_carousel_js',get_template_directory_uri() . '/js/owl.carousel.min.js');
	wp_enqueue_script( 'pressclub_mixitup_js',get_template_directory_uri() . '/js/mixitup.min.js');
	wp_enqueue_media();
	wp_enqueue_script( 'pressclub_custom_js',get_template_directory_uri() . '/js/custom.js');
}
add_action( 'wp_enqueue_scripts', 'pressclub_scripts' );



/* Custom Post Type Class */

class post_type{
	
	public function __construct($name, $singluar_name, $args){
		$this -> register_post_type($name, $singluar_name, $args);	
	}	
	
	//Registering Post Types
	public function register_post_type($name, $singluar_name, $args){ 
		  
	    $args = array_merge(
			array(
				'labels' => array(
							'name' 			=> $name,
							'singular_name' => $singluar_name,
							'add_new'		=> "Add New $singluar_name",
							'add_new_item' 	=> "Add New $singluar_name",
							'edit_item' 	=> "Edit $singluar_name",
							'new_item' 		=> "New $singluar_name",
							'view_item' 	=> "View $singluar_name",
							'search_items' 	=> "Search $name",
							'not_found' 	=> "No $name found",
							'all_items' => "All $name",
							'not_found_in_trash' 	=> "No $name found in Trash", 
							'parent_item_colon' 	=> '',
							'menu_name' 	=>  $name
						  ),
				'public' 	=> true,
				'query_var' => strtolower($singluar_name),
				'hierarchical' => true,
				'rewrite' 	=> array('slug' => $name),
				'menu_icon' =>	admin_url().'images/media-button-video.gif',		 
            //	'supports' 	=> array('title','editor')
				'supports' 	=> array('')
            
			),
			$args  
	    );
		  
		register_post_type(strtolower($name),$args);
	}
	
	//Taxonomies
	public function taxonomies($post_types, $tax_arr){		
		
		$taxonomies = array();

		foreach ($tax_arr as $name => $arr){

			$singular_name = $arr['singular_name'];

			$labels = array(
					'name' => $name,
					'singular_name' => $singular_name,
					'add_new' => "Add New $singular_name",
					'add_new_item' => "Add New $singular_name",
					'edit_item' => "Edit $singular_name",
					'new_item' => "New $singular_name",
					'view_item' => "View $singular_name",
					'update_item' => "Update $singular_name",
					'search_items' => "Search $name",
					'not_found' => "$name Not Found",
					'not_found_trash' => "$name Not Found in Trash",
					'all_items' => "All $name",
					'separate_items_with_comments' => "Separate tags with commas"
			);

			$defaultArr = array(
				'hierarchical' => true,
				'query_var' => true,
				'rewrite' => array('slug' => $name),
				'labels' => $labels	
			);

			$taxonomies[$name] =  array_merge($defaultArr, $arr);	
		}
		
		$this->register_all_taxonomies($post_types, $taxonomies);	
	}
	
	public function register_all_taxonomies($post_types, $taxonomies){	
		foreach($taxonomies as $name => $arr){
			register_taxonomy(strtolower($name),strtolower($post_types), $arr);
		}
	}
}

function init_post_type(){
    
    $committee_arr = array('supports' => array('thumbnail'),);
    $committee_tax_arr =array(
      	"CommitteeCategories"   => 
      	array('singular_name' => 'committee Category','query_var'=> true)
    ); 
    $committee = new post_type('Committee', 'committee', $committee_arr);
    $committee->taxonomies('Committee', $committee_tax_arr);

    $activity_arr = array('supports' => array('title','editor','thumbnail'),);
    $activity = new post_type('Activity', 'activity', $activity_arr);

    $member_arr = array('supports' => array('thumbnail'),);
    $member_tax_arr =array(
      	"memberCategories"   => 
      	array('singular_name' => 'member Category','query_var'=> true)
    ); 
    $member = new post_type('Member', 'member', $member_arr);
    $member->taxonomies('Member', $member_tax_arr);


   	$partner_arr = array('supports' => array('title','editor','thumbnail'),);
    $partner = new post_type('Partner', 'partner', $partner_arr);
    $publication_arr = array('supports' => array('title','editor','thumbnail'),);
    $publication = new post_type('Publication', 'publication', $publication_arr);
    $notice_arr = array('supports' => array('title','editor','thumbnail'),);
    $notice = new post_type('Notice', 'notice', $notice_arr);
    $MediaCoverage_arr = array('supports' => array('title','editor','thumbnail'),);
    $mediaCoverage = new post_type('MediaCoverage', 'mediacoverage', $MediaCoverage_arr);
    $EnlistedMedia_arr = array('supports' => array('thumbnail'),);
    $EnlistedMedia = new post_type('EnlistedMedia', 'enlistedmedia', $EnlistedMedia_arr);
    $Slider_arr = array('supports' => array('title','thumbnail'),);
    $Slider = new post_type('Slider', 'slider', $Slider_arr);
    $Gallery_arr = array('supports' => array('title','thumbnail'),);
    $gallery = new post_type('Gallery', 'gallery', $Gallery_arr);
    $Importweb_arr = array('supports' => array('title','thumbnail'),);
    $Importantweb = new post_type('Importantweb', 'importantweb', $Importweb_arr);
}

add_action('init', 'init_post_type');

add_action( 'admin_init', 'add_meta_boxes' );

function add_meta_boxes() {
    add_meta_box('member_metabox','Member Information','member_field','member');
    add_meta_box('committee_metabox','Committee Information','committee_field','committee');
    add_meta_box('notice_metabox','Notice Information','notice_field','notice');
    add_meta_box('publication_metabox','Publication Information','publication_field','publication');
    add_meta_box('activity_metabox','Activity Information','activity_field','activity');
    add_meta_box('mediacoverage_metabox','mediacoverage Information','mediacoverage_field','mediacoverage');

add_meta_box('slider_metabox','slider Information','slider_field','slider');
}

function slider_field(){
	global $post;
	$slider_link=get_post_meta($post->ID, 'slider_link',true);
?>
		<p class="member_p"> 
		<label for="" class="result_label">slider Link</label> 
		<input type="text" name="slider_link" class="result_text" id="" value="<?php echo $slider_link;?>"> 
	</p>

<?php }


add_action( 'save_post', 'save_slider_field',10,2);

function save_slider_field($post_id, $post){
	global $post;
    if('slider'!= $post->post_type) 
        return ;  
    $slider_link=$_POST['slider_link'];
    update_post_meta($post->ID, 'slider_link', $slider_link);
 }


function mediacoverage_field(){
	global $post;
	$mediacoverage_link=get_post_meta($post->ID, 'mediacoverage_link',true);
?>
		<p class="member_p"> 
		<label for="" class="result_label">mediacoverage Link</label> 
		<input type="text" name="mediacoverage_link" class="result_text" id="" value="<?php echo $mediacoverage_link;?>"> 
	</p>

<?php }


add_action( 'save_post', 'save_mediacoverage_field',10,2);

function save_mediacoverage_field($post_id, $post){
	global $post;
    if('mediacoverage'!= $post->post_type) 
        return ;  
   $mediacoverage_link=$_POST['mediacoverage_link'];
    
    update_post_meta($post->ID, 'mediacoverage_link', $mediacoverage_link);
 }


function activity_field(){
	global $post;
	$activity_link=get_post_meta($post->ID, 'activity_link',true);
?>
		<p class="member_p"> 
		<label for="" class="result_label">Activity Link</label> 
		<input type="text" name="activity_link" class="result_text" id="" value="<?php echo $activity_link;?>"> 
	</p>

<?php }


add_action( 'save_post', 'save_activity_field',10,2);

function save_activity_field($post_id, $post){
	global $post;
    if('activity'!= $post->post_type) 
        return ;  
    $activity_link=$_POST['activity_link'];
    update_post_meta($post->ID, 'activity_link', $activity_link);
 }


function publication_field(){
	global $post;
	$publication_name=get_post_meta($post->ID, 'publication_name',true);
	$publication_writter=get_post_meta($post->ID, 'publication_writter',true);
	$dpwork_thumbinail_img_id = get_post_meta( $post->ID, '_dpwork_thumbinail_img_id', true );
$pdf="";
 if($dpwork_thumbinail_img_id!==""){
 	$image_link = wp_get_attachment_url( $dpwork_thumbinail_img_id,"full");
 	$pdf='<a href="'.$image_link.'" alt="" style="max-width:100%;" download>Download pdf</a>';
 }

?>

	<p class="member_p"> 
		<label for="" class="result_label">Name</label> 
		<input type="text" name="publication_name" class="result_text" id="" value="<?php echo $publication_name;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label"> Writter</label> 
		<input type="text" name="publication_writter" class="result_text" id="" value="<?php echo $publication_writter;?>"> 
	</p>

	<!-- <p class="member_p">
		<label for="">Upload your PDF here.</label>
		<input type="file" id="wp_custom_attachment" name="wp_custom_attachment" value="" size="25" />
	</p>
 -->
 	<style type="text/css">
		.work-thumbinail-container{max-width: 260px; max-height: 260px;overflow: hidden;}
		.work-thumbinail-container img{width: 100%; height: 100%;}
		.hidden{display: none;}
	</style>

	<div class="work-thumbinail-container">
		<?php echo $pdf;?>
	</div>

	<input type="hidden" class="work-thumbinail-img-id" name="dpwork_thumbinail_img_id" value="<?php echo $dpwork_thumbinail_img_id;?>">


	<a href="javascript:void(0);" class="upload-work-thumbinail <?php if($dpwork_thumbinail_img_id!==""){ ?> hidden<?php } ?>">Upload Thumbnail</a>
	<a href="javascript:void(0);" class="delete-work-thumbinail <?php if($dpwork_thumbinail_img_id===""){ ?> hidden<?php } ?>">Remove Thumbnail</a>


	<script type="text/javascript">
		jQuery(function($){

			var frame,
			metaBox = $('#publication_metabox'),
			addImgLink = metaBox.find('.upload-work-thumbinail'),
			delImgLink = metaBox.find('.delete-work-thumbinail'),
			imgContainer = metaBox.find( '.work-thumbinail-container'),
			imgIdInput = metaBox.find('.work-thumbinail-img-id');
			// ADD IMAGE LINK
		  	addImgLink.on( 'click', function( event ){
		  		event.preventDefault();
		  		// If the media frame already exists, reopen it.
		    	if ( frame ) {
		      		frame.open();
		      		return;
		      	}
		      	// Create a new media frame
		    	frame = wp.media({
		      		title: 'Select or Upload Media for your work Thumbnail',
		      		button: {
		        		text: 'Add this PDF'
		      		},
		      		multiple: false  // Set to true to allow multiple files to be selected
		    	});
		    	frame.on( 'select', function() {
		    		// Get media attachment details from the frame state
		    	    var attachment = frame.state().get('selection').first().toJSON();

		    	    // Send the attachment URL to our custom image input field.
		    	    imgContainer.append( '<a href="'+attachment.url+'" alt="" style="max-width:100%;" download>PDF DOWNLOAD</a>' );
		    	    
		    	    // Send the attachment id to our hidden input
		    	    imgIdInput.val( attachment.id );

		    	    // Hide the add image link
		    	    addImgLink.addClass( 'hidden' );

		    	    // Unhide the remove image link
		    	    delImgLink.removeClass( 'hidden' );
		    	});

		    	// Finally, open the modal on click
		    	frame.open();
		    });

		    // DELETE IMAGE LINK
	      	delImgLink.on( 'click', function( event ){

	        	event.preventDefault();

	        	// Clear out the preview image
	        	imgContainer.html( '' );

	        	// Un-hide the add image link
	        	addImgLink.removeClass( 'hidden' );

	        	// Hide the delete image link
	        	delImgLink.addClass( 'hidden' );

	        	// Delete the image id from the hidden input
	        	imgIdInput.val( '' );

	      });
		});
	</script>

<?php

}


add_action( 'save_post', 'save_publication_field',10,2);

function save_publication_field($post_id, $post){
	global $post;
    if('publication'!= $post->post_type) 
        return ;  
    
    $publication_name=$_POST['publication_name'];
    $publication_writter=$_POST['publication_writter'];

    $dpwork_thumbinail_img_id = "";

	if(isset($_POST["dpwork_thumbinail_img_id"]))
        {
            $dpwork_thumbinail_img_id = $_POST["dpwork_thumbinail_img_id"];
        } 
        update_post_meta($post_id, "_dpwork_thumbinail_img_id", $dpwork_thumbinail_img_id);

    update_post_meta($post->ID, 'publication_name', $publication_name);
    update_post_meta($post->ID, 'publication_writter', $publication_writter);

 }

function notice_field(){
	global $post;
	$notice_time=get_post_meta($post->ID, 'notice_time',true);
	$notice_location=get_post_meta($post->ID, 'notice_location',true);
	$notice_subject=get_post_meta($post->ID, 'notice_subject',true);

?>

	<p class="member_p"> 
		<label for="" class="result_label">Time</label> 
		<input type="text" name="notice_time" class="result_text" id="" value="<?php echo $notice_time;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label"> Location</label> 
		<input type="text" name="notice_location" class="result_text" id="" value="<?php echo $notice_location;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Subject</label> 
		<input type="text" name="notice_subject" class="result_text" id="" value="<?php echo $notice_subject;?>"> 
	</p>


	<?php
}


add_action( 'save_post', 'save_notice_field',10,2);

function save_notice_field($post_id, $post){
	global $post;
    if('notice'!= $post->post_type) 
        return ;  
    
    $notice_time=$_POST['notice_time'];
    $notice_location=$_POST['notice_location'];
    $notice_subject=$_POST['notice_subject'];

    update_post_meta($post->ID, 'notice_time', $notice_time);
    update_post_meta($post->ID, 'notice_location', $notice_location);
    update_post_meta($post->ID, 'notice_subject', $notice_subject);

 }

function member_field(){ 
	
	global $post;

	$member_name_english=get_post_meta($post->ID, 'member_name_english',true);
	$member_press_designation_english=get_post_meta($post->ID, 'member_press_designation_english',true);
	$member_fb=get_post_meta($post->ID, 'member_fb',true);

	$member_name=get_post_meta($post->ID, 'member_name',true);
	echo $member_fname=get_post_meta($post->ID, 'member_fname',true);
	$member_rmedia=get_post_meta($post->ID, 'member_rmedia',true);
	$member_status=get_post_meta($post->ID, 'member_status',true);
	$member_birth=get_post_meta($post->ID, 'member_birth',true);
	$member_present_address=get_post_meta($post->ID, 'member_present_address',true);
	$member_pemanent_address=get_post_meta($post->ID, 'member_pemanent_address',true);
	$member_email=get_post_meta($post->ID, 'member_email',true);
	$member_personal_cell=get_post_meta($post->ID, 'member_personal_cell',true);
	$member_personal_office=get_post_meta($post->ID, 'member_personal_office',true);
	$member_job=get_post_meta($post->ID, 'member_job',true);
	$member_education=get_post_meta($post->ID, 'member_education',true);
	$member_blood=get_post_meta($post->ID, 'member_blood',true);
	$member_other_organization=get_post_meta($post->ID, 'member_other_organization',true);

?>
	
	<p class="member_p"> 
		<label for="" class="result_label"> Full Name (English)</label> 
		<input type="text" name="member_name_english" class="result_text" id="" value="<?php echo $member_name_english;?>"> 
	</p>

	<p class="member_p">
		<label for="" class="result_label"> PressClub Designation (English)</label> 
		<input type="text" name="member_press_designation_english" class="result_text" id="" value="<?php echo $member_press_designation_english;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Member Facebook (link)</label> 
		<input type="text" name="member_fb" class="result_text" id="" value="<?php echo $member_fb;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label"> Full Name (Bangla)</label> 
		<input type="text" name="member_name" class="result_text" id="" value="<?php echo $member_name;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label"> Father's Name / Spouse Name</label> 
		<input type="text" name="member_fname" class="result_text" id="" value="<?php echo $member_fname;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Represented Media</label> 
		<input type="text" name="member_rmedia" class="result_text" id="" value="<?php echo $member_rmedia;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Membership Status</label> 
		<input type="text" name="member_status" class="result_text" id="" value="<?php echo $member_status;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Date of Birth</label> 
		<input type="text" name="member_birth" class="result_text" id="" value="<?php echo $member_birth;?>"> 
	</p>
	
	<p class="member_p"> 
		<label for="" class="result_label">Present Address</label> 
		<input type="text" name="member_present_address" class="result_text" id="" value="<?php echo $member_present_address;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Permanent Address</label> 
		<input type="text" name="member_pemanent_address" class="result_text" id="" value="<?php echo $member_pemanent_address;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Email</label> 
		<input type="text" name="member_email" class="result_text" id="" value="<?php echo $member_email;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Cell (Personal)</label> 
		<input type="text" name="member_personal_cell" class="result_text" id="" value="<?php echo $member_personal_cell;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Cell (Office)</label> 
		<input type="text" name="member_personal_office" class="result_text" id="" value="<?php echo $member_personal_office;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Job History</label> 
		<input type="text" name="member_job" class="result_text" id="" value="<?php echo $member_job;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Education</label> 
		<input type="text" name="member_education" class="result_text" id="" value="<?php echo $member_education;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Blood Group</label> 
		<input type="text" name="member_blood" class="result_text" id="" value="<?php echo $member_blood;?>"> 
	</p>

	<p class="member_p"> 
		<label for="" class="result_label">Other Organizations (if any)</label> 
		<input type="text" name="member_other_organization" class="result_text" id="" value="<?php echo $member_other_organization;?>"> 
	</p>
	
<?php }


add_action( 'save_post', 'save_member_field',10,2);

function save_member_field($post_id, $post){
	global $post;
    if('member'!= $post->post_type) 
        return ;  

    $member_name_english=$_POST['member_name_english'];
	$member_press_designation_english=$_POST['member_press_designation_english'];
	$member_fb=$_POST['member_fb'];


    $member_name=$_POST['member_name'];
    $member_fname=$_POST['member_fname'];
    $member_rmedia=$_POST['member_rmedia'];
    $member_status=$_POST['member_status'];
    $member_birth=$_POST['member_birth'];
    $member_present_address=$_POST['member_present_address'];
    $member_pemanent_address=$_POST['member_pemanent_address'];
    $member_email=$_POST['member_email'];
    $member_personal_cell=$_POST['member_personal_cell'];
    $member_personal_office=$_POST['member_personal_office'];
    $member_job=$_POST['member_job'];
    $member_education=$_POST['member_education'];
    $member_blood=$_POST['member_blood'];
    $member_other_organization=$_POST['member_other_organization'];


    update_post_meta($post->ID, 'member_name_english', $member_name_english);
    update_post_meta($post->ID, 'member_press_designation_english', $member_press_designation_english);
    update_post_meta($post->ID, 'member_fb', $member_fb);
    update_post_meta($post->ID, 'member_name', $member_name);
    update_post_meta($post->ID, 'member_fname', $member_fname);
    update_post_meta($post->ID, 'member_rmedia', $member_rmedia);
    update_post_meta($post->ID, 'member_status', $member_status);
    update_post_meta($post->ID, 'member_birth', $member_birth);
    update_post_meta($post->ID, 'member_present_address', $member_present_address);
    update_post_meta($post->ID, 'member_pemanent_address', $member_pemanent_address);
    update_post_meta($post->ID, 'member_email', $member_email);
    update_post_meta($post->ID, 'member_personal_cell', $member_personal_cell);
    update_post_meta($post->ID, 'member_personal_office', $member_personal_office);
    update_post_meta($post->ID, 'member_job', $member_job);
    update_post_meta($post->ID, 'member_education', $member_education);
    update_post_meta($post->ID, 'member_blood', $member_blood);
    update_post_meta($post->ID, 'member_other_organization', $member_other_organization);
}


function committee_field(){ 
	
	global $post;

	$committee_name_english=get_post_meta($post->ID, 'committee_name_english',true);
	$committee_press_designation_english=get_post_meta($post->ID, 'committee_press_designation_english',true);
	$committee_fb=get_post_meta($post->ID, 'committee_fb',true);

	$committee_name=get_post_meta($post->ID, 'committee_name',true);
	$committee_fname=get_post_meta($post->ID, 'committee_fname',true);
	$committee_rmedia=get_post_meta($post->ID, 'committee_rmedia',true);
	$committee_status=get_post_meta($post->ID, 'committee_status',true);
	$committee_birth=get_post_meta($post->ID, 'committee_birth',true);
	$committee_present_address=get_post_meta($post->ID, 'committee_present_address',true);
	$committee_pemanent_address=get_post_meta($post->ID, 'committee_pemanent_address',true);
	$committee_email=get_post_meta($post->ID, 'committee_email',true);
	$committee_personal_cell=get_post_meta($post->ID, 'committee_personal_cell',true);
	$committee_personal_office=get_post_meta($post->ID, 'committee_personal_office',true);
	$committee_job=get_post_meta($post->ID, 'committee_job',true);
	$committee_education=get_post_meta($post->ID, 'committee_education',true);
	$committee_blood=get_post_meta($post->ID, 'committee_blood',true);
	$committee_other_organization=get_post_meta($post->ID, 'committee_other_organization',true);

?>

	<p class="committee_p"> 
		<label for="" class="result_label"> Full Name (English)</label> 
		<input type="text" name="committee_name_english" class="result_text" id="" value="<?php echo $committee_name_english;?>"> 
	</p>

	<p class="committee_p">
		<label for="" class="result_label"> PressClub Designation (English)</label> 
		<input type="text" name="committee_press_designation_english" class="result_text" id="" value="<?php echo $committee_press_designation_english;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">committee Facebook (link)</label> 
		<input type="text" name="committee_fb" class="result_text" id="" value="<?php echo $committee_fb;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label"> Full Name (Bangla)</label> 
		<input type="text" name="committee_name" class="result_text" id="" value="<?php echo $committee_name;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label"> Father's Name / Spouse Name</label> 
		<input type="text" name="committee_fname" class="result_text" id="" value="<?php echo $committee_fname;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Represented Media</label> 
		<input type="text" name="committee_rmedia" class="result_text" id="" value="<?php echo $committee_rmedia;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">committeeship Status</label> 
		<input type="text" name="committee_status" class="result_text" id="" value="<?php echo $committee_status;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Date of Birth</label> 
		<input type="text" name="committee_birth" class="result_text" id="" value="<?php echo $committee_birth;?>"> 
	</p>
	
	<p class="committee_p"> 
		<label for="" class="result_label">Present Address</label> 
		<input type="text" name="committee_present_address" class="result_text" id="" value="<?php echo $committee_present_address;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Permanent Address</label> 
		<input type="text" name="committee_pemanent_address" class="result_text" id="" value="<?php echo $committee_pemanent_address;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Email</label> 
		<input type="text" name="committee_email" class="result_text" id="" value="<?php echo $committee_email;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Cell (Personal)</label> 
		<input type="text" name="committee_personal_cell" class="result_text" id="" value="<?php echo $committee_personal_cell;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Cell (Office)</label> 
		<input type="text" name="committee_personal_office" class="result_text" id="" value="<?php echo $committee_personal_office;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Job History</label> 
		<input type="text" name="committee_job" class="result_text" id="" value="<?php echo $committee_job;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Education</label> 
		<input type="text" name="committee_education" class="result_text" id="" value="<?php echo $committee_education;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Blood Group</label> 
		<input type="text" name="committee_blood" class="result_text" id="" value="<?php echo $committee_blood;?>"> 
	</p>

	<p class="committee_p"> 
		<label for="" class="result_label">Other Organizations (if any)</label> 
		<input type="text" name="committee_other_organization" class="result_text" id="" value="<?php echo $committee_other_organization;?>"> 
	</p>
	
<?php }


add_action( 'save_post', 'save_committee_field',10,2);

function save_committee_field($post_id, $post){
	global $post;
    if('committee'!= $post->post_type) 
        return ;  

    $committee_name_english=$_POST['committee_name_english'];
	$committee_press_designation_english=$_POST['committee_press_designation_english'];
	$committee_fb=$_POST['committee_fb'];

    
    $committee_name=$_POST['committee_name'];
    $committee_fname=$_POST['committee_fname'];
    $committee_birth=$_POST['committee_birth'];

    $committee_rmedia=$_POST['committee_rmedia'];

    $committee_status=$_POST['committee_status'];

    $committee_present_address=$_POST['committee_present_address'];
    $committee_pemanent_address=$_POST['committee_pemanent_address'];
    $committee_email=$_POST['committee_email'];
    $committee_personal_cell=$_POST['committee_personal_cell'];
    $committee_personal_office=$_POST['committee_personal_office'];
    $committee_job=$_POST['committee_job'];
    $committee_education=$_POST['committee_education'];
    $committee_blood=$_POST['committee_blood'];
    $committee_other_organization=$_POST['committee_other_organization'];


    update_post_meta($post->ID, 'committee_name_english', $committee_name_english);
    update_post_meta($post->ID, 'committee_press_designation_english', $committee_press_designation_english);
    update_post_meta($post->ID, 'committee_fb', $committee_fb);


    update_post_meta($post->ID, 'committee_name', $committee_name);
    update_post_meta($post->ID, 'committee_fname', $committee_fname);
    update_post_meta($post->ID, 'committee_birth', $committee_birth);

    update_post_meta($post->ID, 'committee_rmedia', $committee_rmedia);

    update_post_meta($post->ID, 'committee_status', $committee_status);


    update_post_meta($post->ID, 'committee_present_address', $committee_present_address);
    update_post_meta($post->ID, 'committee_pemanent_address', $committee_pemanent_address);
    update_post_meta($post->ID, 'committee_email', $committee_email);
    update_post_meta($post->ID, 'committee_personal_cell', $committee_personal_cell);
    update_post_meta($post->ID, 'committee_personal_office', $committee_personal_office);
    update_post_meta($post->ID, 'committee_job', $committee_job);
    update_post_meta($post->ID, 'committee_education', $committee_education);
    update_post_meta($post->ID, 'committee_blood', $committee_blood);
    update_post_meta($post->ID, 'committee_other_organization', $committee_other_organization);
}


function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  global $paged;
  
  if (empty($paged)) {
    $paged = 1;
  }
  
  if($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination'>";
      echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}


function wpse_load_custom_search_template(){ 

	if($_REQUEST['type']=== 'member' ) { 
		require('member-search.php'); 
		die(); 
	} 

	if($_REQUEST['type'] === 'committee' ) { 
		require('committee-search.php'); 
		die(); 
	} 
} 

add_action('init','wpse_load_custom_search_template'); 


add_action( 'customize_register', 'pressclub_customize_register' );

function pressclub_customize_register($wp_customize){

	

	$wp_customize->add_section('President',array(
		'title'=>'সভাপতি',
		'priority'=>10,
	));

	$wp_customize->add_setting('name_president', array(
	  'default' => '',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'name_president', array(
	  'label' => __( 'Name of president' ),
	  'section' => 'President',
	  'type'=>'text'
	) );

	$wp_customize->add_setting('desgination_president', array(
	  'default' => '',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'desgination_president', array(
	  'label' => __( 'Desgination of president' ),
	  'section' => 'President',
	  'type'=>'text'
	) );

	$wp_customize->add_setting('spech_president', array(
	  'default' => '',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'spech_president', array(
	  'label' => __( 'Spech of president' ),
	  'section' => 'President',
	  'type'=>'textarea'
	) );


	$wp_customize->add_section('secratary',array(
		'title'=>'সাধারণ সম্পাদক',
		'priority'=>10,
	));

	$wp_customize->add_setting('name_secratary', array(
	  'default' => '',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'name_secratary', array(
	  'label' => __( 'Name of secratary' ),
	  'section' => 'secratary',
	  'type'=>'text'
	) );

	$wp_customize->add_setting('desgination_secratary', array(
	  'default' => '',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'desgination_secratary', array(
	  'label' => __( 'Desgination of secratary' ),
	  'section' => 'secratary',
	  'type'=>'text'
	) );

	$wp_customize->add_setting('spech_secratary', array(
	  'default' => '',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'spech_secratary', array(
	  'label' => __( 'Spech of secratary' ),
	  'section' => 'secratary',
	  'type'=>'textarea'
	) );
	/*$wp_customize->add_setting();
	$wp_customize->add_control();*/


	$wp_customize->add_section('logo_section',array(
		'title'=>'Upload Your Logo',
		'priority'=>10,
	));

	$wp_customize->add_setting('logo_upload', array(
	  'default' => '',
	  'transport'=>'postMessage'
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize,'logo_upload', array(
	  'label' => __( 'Logo Upload' ),
	  'section' => 'logo_section',
	  'settings'=>'logo_upload'	
	)));


	$wp_customize->add_section('Social_section',array(
		'title'=>'Social Media Section',
		'priority'=>10,
	));

	$wp_customize->add_setting('fb_section', array(
	  'default' => 'https://www.facebook.com/',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'fb_section', array(
	  'label' => __( 'FB Link' ),
	  'section' => 'Social_section',
	  'type'=>'text'
	) );


	$wp_customize->add_setting('twitter_section', array(
	  'default' => 'https://twitter.com/',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'twitter_section', array(
	  'label' => __( 'Twitter Link' ),
	  'section' => 'Social_section',
	  'type'=>'text'
	) );


	$wp_customize->add_setting('youtube_section', array(
	  'default' => 'https://www.youtube.com/',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'youtube_section', array(
	  'label' => __( 'Youtube Link' ),
	  'section' => 'Social_section',
	  'type'=>'text'
	) );



	$wp_customize->add_setting('linkedin_section', array(
	  'default' => 'https://www.linkedin.com/',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'linkedin_section', array(
	  'label' => __( 'Linkedin Link' ),
	  'section' => 'Social_section',
	  'type'=>'text'
	) );

	$wp_customize->add_setting('google_section', array(
	  'default' => 'https://www.google.com/',
	  'transport'=>'refresh'
	) );

	$wp_customize->add_control( 'google_section', array(
	  'label' => __( 'Google Link' ),
	  'section' => 'Social_section',
	  'type'=>'text'
	) );

	$wp_customize->add_setting('fb_page_section', array(
	  'default' => 'https://www.facebook.com/',
	  'transport'=>'refresh'
	));

	$wp_customize->add_control( 'fb_page_section', array(
	  'label' => __( 'Facebook Page Link' ),
	  'section' => 'Social_section',
	  'type'=>'text'
	));


	$wp_customize->add_section('notice_section',array(
		'title'=>' Notice Section',
		'priority'=>10,
	));

	$wp_customize->add_setting('notice_page_section', array(
	  'default' => '12',
	  'transport'=>'refresh'
	));

	$wp_customize->add_control( 'notice_page_section', array(
	  'label' => __( 'Notice Per page' ),
	  'section' => 'notice_section',
	  'type'=>'text'
	) );


	$wp_customize->add_section('Publication_section',array(
		'title'=>' Publication Section',
		'priority'=>10,
	));

	$wp_customize->add_setting('Publication_page_section', array(
	  'default' => '12',
	  'transport'=>'refresh'
	));

	$wp_customize->add_control( 'Publication_page_section', array(
	  'label' => __( 'Publication Per page' ),
	  'section' => 'Publication_section',
	  'type'=>'text'
	) );

	$wp_customize->add_section('mediacoverage_section',array(
		'title'=>' mediacoverage Section',
		'priority'=>10,
	));

	$wp_customize->add_setting('mediacoverage_page_section', array(
	  'default' => '12',
	  'transport'=>'refresh'
	));

	$wp_customize->add_control( 'mediacoverage_page_section', array(
	  'label' => __( 'mediacoverage Per page' ),
	  'section' => 'mediacoverage_section',
	  'type'=>'text'
	) );
}




function add_ajaxurl_cdata_to_front(){ ?>
    <script type="text/javascript"> //<![CDATA[
        ajaxurl = '<?php echo admin_url( 'admin-ajax.php'); ?>';
        previmage= '<?php echo get_template_directory_uri();?>' + '/images/prev.png';
        nextimage= '<?php echo get_template_directory_uri();?>' + '/images/next.png';
    //]]> </script>
<?php }
add_action( 'wp_head', 'add_ajaxurl_cdata_to_front', 1);

// **********************************************************************// 
// ! AJAX Quick View
// **********************************************************************//

add_action('wp_ajax_brand_quick_view', 'brand_quick_view');
add_action('wp_ajax_nopriv_brand_quick_view', 'brand_quick_view');

if(!function_exists('brand_quick_view')) {
	function brand_quick_view() {

		$partner_arr=array();
		if(empty($_POST['brandid'])) {
			echo 'Error: Absent product id';
			die();
		}

		$args = array(
			'p'=>$_POST['brandid'],
			'post_type' => 'partner'
		);

		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) : $the_query->the_post();
				global $post;
			
			
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'pressclub_partner_image' );
			$parr=array('title'=>get_the_title(),'content'=>get_the_content(),
				'image'=>$image[0]);
			endwhile;
			wp_reset_query();
			wp_reset_postdata();
		} else {
			echo 'No posts were found!';
		}

		echo wp_send_json($parr);
		die();
	}
}


add_action('wp_ajax_notice_quick_view', 'notice_quick_view');
add_action('wp_ajax_nopriv_notice_quick_view', 'notice_quick_view');

if(!function_exists('notice_quick_view')) {
	function notice_quick_view() {

		$partner_arr=array();
		if(empty($_POST['noticeid'])) {
			echo 'Error: Absent product id';
			die();
		}

		$args = array(
			'p'=>$_POST['noticeid'],
			'post_type' => 'notice'
		);

		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) : $the_query->the_post();
				global $post;
				$notice_time=get_post_meta($post->ID, 'notice_time',true);
				$notice_location=get_post_meta($post->ID, 'notice_location',true);
				$notice_subject=get_post_meta($post->ID, 'notice_subject',true);
			
			$parr=array('title'=>get_the_title(),'notice_time'=>$notice_time,'notice_location'=>'স্থান:' . $notice_location,
				'notice_subject'=>'সংবাদ সম্মেলনের বিষয়:' . $notice_subject);
			endwhile;
			wp_reset_query();
			wp_reset_postdata();
		} else {
			echo 'No posts were found!';
		}

		echo wp_send_json($parr);
		die();
	}
}


/*function rc_add_cpts_to_search($query) {

	// Check to verify it's search page
	if( is_search() ) {
		$searchable_types = array('notice','activity');
		$query->set( 'post_type', $searchable_types );
	}
	return $query;
}
add_action( 'pre_get_posts', 'rc_add_cpts_to_search' );*/

function menu_fix_on_search_page( $query ) {
   	 if(is_search()){
       $query->set( 'post_type', array('notice', 'activity'));
         return $query;
     }
 }
/*if( !is_admin() ) */
//add_filter( 'pre_get_posts', 'menu_fix_on_search_page' );



add_filter( 'manage_activity_posts_columns', 'set_custom_edit_activity_columns' );
add_action( 'manage_activity_posts_custom_column' , 'custom_activity_column', 10, 2 );

function set_custom_edit_activity_columns($columns) {
	$columns['activity_link'] = __( 'Activity Link', '' );
    return $columns;
}

function custom_activity_column( $column, $post_id ) {
    switch ( $column ) {
        case 'activity_link' :
        	 echo get_post_meta( $post_id ,'activity_link' ,true ); 
        	 break;
    }
}

add_filter( 'manage_member_posts_columns', 'set_custom_edit_member_columns' );
add_action( 'manage_member_posts_custom_column' , 'custom_member_column', 10, 2 );

function set_custom_edit_member_columns($columns) {
	unset( $columns['title'] );
	$columns['member_name'] = __( 'member Name', '' );
    return $columns;
}

function custom_member_column( $column, $post_id ) {
    switch ( $column ) {
        case 'member_name' :
        	 echo get_post_meta( $post_id ,'member_name' ,true ); 
        	 break;
    }
}


add_filter( 'manage_partner_posts_columns', 'set_custom_edit_partner_columns' );
add_action( 'manage_partner_posts_custom_column' , 'custom_partner_column', 10, 2 );

function set_custom_edit_partner_columns($columns) {
    $columns['featured_thumb'] = __( 'Thumbnail', '' );
    return $columns;
}

function custom_partner_column( $column, $post_id ) {
    switch ( $column ) {
        case 'featured_thumb' :
        	 echo the_post_thumbnail( 'pressclub-thumbnail-avatar' );
        	 break;
    }
}


add_filter( 'manage_publication_posts_columns', 'set_custom_edit_publication_columns' );
add_action( 'manage_publication_posts_custom_column' , 'custom_publication_column', 10, 2 );

function set_custom_edit_publication_columns($columns) {
	unset( $columns['title'] );
	$columns['publication_name'] = __( 'publication Name', '' );
	$columns['publication_writter'] = __( 'publication Writter', '' );
    return $columns;
}

function custom_publication_column( $column, $post_id ) {
    switch ( $column ) {
        case 'publication_name' :
        	 echo get_post_meta( $post_id ,'publication_name' ,true ); 
        	 break;
        case 'publication_name' :
        	 echo get_post_meta( $post_id ,'publication_writter' ,true ); 
        	 break;
    }
}

add_filter( 'manage_committee_posts_columns', 'set_custom_edit_committee_columns' );
add_action( 'manage_committee_posts_custom_column' , 'custom_committee_column', 10, 2 );

function set_custom_edit_committee_columns($columns) {
	unset( $columns['title'] );
	$columns['committee_name'] = __( 'committee Name', '' );
    return $columns;
}

function custom_committee_column( $column, $post_id ) {
    switch ( $column ) {
        case 'committee_name' :
        	 echo get_post_meta( $post_id ,'committee_name' ,true ); 
        	 break;
    }
}

add_filter( 'manage_notice_posts_columns', 'set_custom_edit_notice_columns' );
add_action( 'manage_notice_posts_custom_column' , 'custom_notice_column', 10, 2 );

function set_custom_edit_notice_columns($columns) {
	$columns['notice_time'] = __( 'notice Time', '' );
	$columns['notice_location'] = __( 'notice Location', '' );
	$columns['notice_subject'] = __( 'notice Subject', '' );
    return $columns;
}

function custom_notice_column( $column, $post_id ) {
    switch ( $column ) {
        case 'notice_time' :
        	 echo get_post_meta( $post_id ,'notice_time' ,true ); 
        	 break;
       	case 'notice_location' :
        	 echo get_post_meta( $post_id ,'notice_location' ,true ); 
        	 break;
       	case 'notice_subject' :
        	 echo get_post_meta( $post_id ,'notice_subject' ,true ); 
        	 break;
    }
}

add_filter( 'manage_enlistedmedia_posts_columns', 'set_custom_edit_enlistedmedia_columns' );
add_action( 'manage_enlistedmedia_posts_custom_column' , 'custom_enlistedmedia_column', 10, 2 );

function set_custom_edit_enlistedmedia_columns($columns) {
    $columns['featured_thumb'] = __( 'Thumbnail', '' );
    return $columns;
}

function custom_enlistedmedia_column( $column, $post_id ) {
    switch ( $column ) {
        case 'featured_thumb' :
        	 echo the_post_thumbnail( 'pressclub-thumbnail-avatar' );
        	 break;
    }
}


add_filter( 'manage_importantweb_posts_columns', 'set_custom_edit_importantweb_columns' );
add_action( 'manage_importantweb_posts_custom_column' , 'custom_importantweb_column', 10, 2 );

function set_custom_edit_importantweb_columns($columns) {
    $columns['featured_thumb'] = __( 'Thumbnail', '' );
    return $columns;
}

function custom_importantweb_column( $column, $post_id ) {
    switch ( $column ) {
        case 'featured_thumb' :
        	 echo the_post_thumbnail( 'pressclub-thumbnail-avatar' );
        	 break;
    }
}



function get_rok_post($post_type,$posts_per_page,$pagination,$template_first_tag='',$template_last_tag='',$template_name='',$w_args=''){
	    
	    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
        $query_args = array(
        	'post_type' => $post_type,
        	'posts_per_page' => $posts_per_page,
          	'paged'=>$paged
        
        );

        if(!empty($w_args)){
        	$query_args=$w_args;
        	
        }
        $the_query = new WP_Query( $query_args );

        if(!empty($template_first_tag)){
        	echo $template_first_tag;
        }
    	
    	if($the_query->have_posts()): 

            	while($the_query->have_posts()) : $the_query->the_post(); 
            		if(empty($template_name)){
            			get_template_part( 'template-parts/content', $post_type);
            		}else{
            			get_template_part( 'template-parts/content', $template_name);
            		}
                	
    		    endwhile;

            wp_reset_postdata();

	           if($pagination){
	          		wp_pagenavi( array( 'query' => $the_query ) ); 
	           }else{

	           }
		    else: 

	    endif;

	    if(!empty($template_first_tag)){
        	echo $template_last_tag;
        }
}


function get_cm($post_type='',$taxonomy = ''){  
                $taxonomy = $taxonomy;
                $terms = get_terms( array('taxonomy' => $taxonomy, 'hide_empty' => true)); // Get all terms of a taxonomy
                if($terms):
                    $i=1;
                  foreach ( $terms as $term ) { ?>
                    
                    <div class="panel panel-default">
                        
                        <div class="header">
                            <div class="panel-heading">
                                <a class="panel-title heading-4" data-toggle="collapse" data-parent="#panel-473390" href="#panel-element-<?php echo $i;?>">
                                    <i class="fa fa-bars iconzz"></i> <?php echo $term->name; ?> </a>
                            </div>
                        </div>

                        <div id="panel-element-<?php echo $i;?>" class="panel-collapse collapse in">

                        <?php     

                            $paged=(get_query_var('paged')) ? get_query_var('paged') : 1;    
                            
                            $query_args = array(
                                'post_type' => $post_type,
                                'posts_per_page' => -1,
                              /*  'committeecategories' => $term->term_id*/
                               'tax_query' => array(
                                    array(
                                        'taxonomy' => $taxonomy,
                                        'terms' => $term->term_id,
                                        'field' => 'term_id',
                                    )
                                ),
                            );
                           
                            $the_query = new WP_Query( $query_args );
                            
                            if($the_query->have_posts()): 

                        ?>

                                    
                                        <div class="panel-body">
                                            <section class="club-activities">
                                                <div class="owl-carousel activities-slider2">
                                                          
                                                        <?php      while($the_query->have_posts()) : $the_query->the_post(); 
                                                                    global $post;
                                                                        get_template_part( 'template-parts/content',$post_type);
                                                                endwhile;
                                                        ?> 
                                                      </div>
                                            </section>
                                        </div>
                                           
                        

                        <?php     
                                    else: 
                               endif; 

                        ?>
                        </div>
                    </div>
                <?php $i++;  } ?>

            <?php endif;?>


<?php }


function get_roks_meun($location="",$items_wrap=""){
	$defaults = array(
        'theme_location'  => $location,
        'items_wrap'      => $items_wrap,
    );
    wp_nav_menu( $defaults );
}